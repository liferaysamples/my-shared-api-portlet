# my-shared-api-portlet

This portlet calls a function of a public shared api which is packaged and deployed in a different module.

In this README file you will find everything needed to build, deploy, start this bundle and test this against the public api and its implementation. 

## Getting Started

This project was generated with 

	blade create -b maven -t api my-shared-api 

All comments in the code start with a ``COMMENT:`` so they can easily be found via search function.

### Prerequisites

* Running Liferay 7.1 CE
* Activated Gogo Shell access via command line (add ``module.framework.properties.osgi.console=localhost:11311`` in your ``portal-ext.properties`` file)
* Maven 3
* Java 8
* Project my-shared-api has been built, deployed and started
* Project my-shared-impl has been built, deployed and started

### Installing

Build with

	# mvn clean package

Deploy it with

	# telnet 127.0.0.1 11311
	g! install file:///PATH_TO_FILE
	g! start BUNLDE_ID

Replace the variable ``PATH_TO_FILE`` with the absolute path to the jar file (in target folder of the project) and the ``BUNDLE_ID`` with the bundle id Gogo Shell returns when the bundle was successfully installed.

### Extended Files

* [pom.xml](pom.xml) - added dependencies
* [bnd.bnd](bnd.bnd) - added unique bundle symbolic name 
* [MySharedApiPortlet.java](src/main/java/space/manhart/demo/shared/portlet/MySharedApiPortlet.java) - implemented controller, injected & called service, added attributes to request for view.jsp
* [view.jsp](src/main/resources/META-INF/resources/view.jsp) - displayed attributes, called service
* [Language.properties](src/main/resources/content/Language.properties) - customized names

### Testing

If deployed and started, you can find the Portlet in the category ``Sample`` with the name ``MySharedApi``.

After you added the portlet to a page, you will see the header ``MySharedApi-Portlet Header`` and below a ``Hello USERNAME!``.

The second line only appears if a user is logged in, and if the api and impl are in place. Also you will see some log statements in your ``catalina.out`` logfile. 

## Built With

* [Blade](https://dev.liferay.com/develop/tutorials/-/knowledge_base/7-1/blade-cli) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Liferay](https://dev.liferay.com/) - Portal / runtime container

## Contributing

No contributing since this is a sample project.

## Versioning

No versioning since this is a sample project.

## Authors

* **Manuel Manhart** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
