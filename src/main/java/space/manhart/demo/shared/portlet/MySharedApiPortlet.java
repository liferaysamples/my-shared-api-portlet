package space.manhart.demo.shared.portlet;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalUtil;

import space.manhart.demo.shared.api.MySharedServiceProvider;
import space.manhart.demo.shared.constants.MySharedApiPortletKeys;

/**
 * This is an Spring MVC Portlet generated via Blade to show how to call an
 * external shared API.
 * 
 * @author Manuel Manhart
 */
// @formatter:off
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + MySharedApiPortletKeys.MySharedApi,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
// @formatter:on
public class MySharedApiPortlet extends MVCPortlet {

	// COMMENT: changes start
	/**
	 * COMMENT: Autowire the public service api (is an osgi bean) for use in this
	 * class
	 */
	@Reference
	private MySharedServiceProvider service;
	// COMMENT: changes end

	/**
	 * The render method calls the service
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		// COMMENT: changes start

		// COMMENT: example call of the service from a Java Class
		service.greet("Portlet Class");

		// COMMENT: add the service into the request for use in the jsp
		renderRequest.setAttribute("service", service);

		// COMMENT: example of how to call a service and add only the results for use in
		// the jsp (MVC pattern)
		String greeting = "";
		try {
			User u = PortalUtil.getUser(renderRequest);
			String userName = "Guest";
			if (u != null) {
				userName = u.getFirstName() + " " + u.getLastName();
			}
			greeting = service.getGreeting(userName);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		renderRequest.setAttribute("greeting", greeting);
		// COMMENT: changes end

		super.render(renderRequest, renderResponse);
	}
}