<%@page import="space.manhart.demo.shared.api.MySharedServiceProvider"%>
<%@ include file="/init.jsp" %>

<p>
	<b><liferay-ui:message key="mysharedapi.caption"/></b>
</p>
<%-- COMMENT: changes start --%>

<%-- COMMENT: direct use of the api --%>
<% ((MySharedServiceProvider)request.getAttribute("service")).greet("jsp page"); %>

<%-- COMMENT: use of the api results of the controller --%>
<p>
	${greeting}
</p>

<%-- COMMENT: changes end --%>
